const specialLogging = function() {
    const main = window.onload;
    window.onload = function() {
        try {
            main();
        } catch (e) {
            alert('Error message: ' + e + '\nLine Number: ' + e.lineNumber);
            throw e;
        }
    };
};


const sleep = function(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


let frameTime = 10;
let cardWidth = '100px';


/*const log = function(x) {
    let element = document.createElement("div");
    element.innerHTML = x;
    document.body.appendChild(element);
};*/


const countUp = function*(start) {
    for (n = start; true; n++) {
        yield n;
    }
};


const shuffleArray = function(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
};


const Card = class {
    
    constructor(suit, value, reverse, hidden) {
        
        // suits are "S", "H", "D", and "C"
        this.suit = suit;
        
        this.value = value;
        this.id = Card.idGenerator.next();
        this.element = document.createElement('div');
        this.element.setAttribute('id', this.id.toString());
        if (reverse) {
            this.reversed = true;
        } else {
            this.reversed = false;
        }
        if (hidden) {
            this.hidden = true;
        } else {
            this.hidden = false;
        }

        this.draggable = false;
        this.dragData = {
            dragStart: this.dragStart.bind(this),
            drag: this.drag.bind(this),
            dragEnd: this.dragEnd.bind(this),
            xOffset: 0,
            yOffset: 0,
            active: false,
            attachedCard: null,
        };
        
        this.element.style.width = cardWidth;
        //this.element.style.height = "177.8px";
        this.element.style.borderRadius = '5px';
        this.element.style.position = 'absolute';
        this.setVisibility();
        this.relocate(0, 0, false);
        
        this.image = document.createElement('img');
        this.setImage();
        this.image.style.width = cardWidth;
        this.element.appendChild(this.image);
        this.image.ondragstart = () => false;
        
        document.body.appendChild(this.element);
    }

    attachCard(card) {
        this.dragData.attachedCard = card;
    }

    detachCard() {
        this.dragData.attachedCard = null;
    }

    cascadeZIndex(z) {
        this.dragData.oldZIndex = this.element.style.zIndex;
        this.zIndex(z);
        if (this.dragData.attachedCard !== null) {
            this.dragData.attachedCard.cascadeZIndex(z + 1);
        }
    }

    dragStart(e) {
        this.cascadeZIndex(1000);
        if (e.type === 'touchstart') {
            this.dragData.initialX = e.touches[0].clientX - this.dragData.xOffset;
            this.dragData.initialY = e.touches[0].clientY - this.dragData.yOffset;
        } else {
            this.dragData.initialX = e.clientX - this.dragData.xOffset;
            this.dragData.initialY = e.clientY - this.dragData.yOffset;
        }

        this.dragData.active = true;
    }

    dragEnd(e) {
        this.dragData.initialX = this.dragData.currentX;
        this.dragData.initialY = this.dragData.currentY;

        this.dragData.active = false;

        this.resetZIndex();

        this.dragData.movedCallback(this);
    }

    drag(e) {
        if (this.dragData.active) {
            
            e.preventDefault();
        
            if (e.type === "touchmove") {
                this.dragData.currentX = e.touches[0].clientX - this.dragData.initialX;
                this.dragData.currentY = e.touches[0].clientY - this.dragData.initialY;
            } else {
                this.dragData.currentX = e.clientX - this.dragData.initialX;
                this.dragData.currentY = e.clientY - this.dragData.initialY;
            }

            this.dragData.xOffset = this.dragData.currentX;
            this.dragData.yOffset = this.dragData.currentY;

            this.setTranslate(this.dragData.currentX, this.dragData.currentY);
        }
    }

    resetZIndex() {
        this.zIndex(this.dragData.oldZIndex);
        if (this.dragData.attachedCard !== null) {
            this.dragData.attachedCard.resetZIndex();
        }
    }

    cascadeRelocate(x, y) {
        this.relocate(x, y, true);
        if (this.dragData.attachedCard !== null) {
            this.dragData.attachedCard.cascadeRelocate(x, y);
        }
    }

    clearTransform() {
        this.element.style.transform = '';
        if (this.dragData.attachedCard !== null) {
            this.dragData.attachedCard.clearTransform();
        }
        this.dragData.xOffset = 0;
        this.dragData.yOffset = 0;

    }

    setTranslate(xPos, yPos) {
        this.element.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
        if (this.dragData.attachedCard !== null) {
            this.dragData.attachedCard.setTranslate(xPos, yPos);
        }
    }

    toggleDraggable(draggable, movedCallback) {
        if (movedCallback !== undefined) {
            this.dragData.movedCallback = movedCallback;
        }
        if (draggable === undefined) {
            this.draggable = !this.draggable;
        } else {
            this.draggable = draggable;
        }
        if (this.draggable) {
            this.element.addEventListener('touchstart', this.dragData.dragStart, false);
            this.element.addEventListener('touchend', this.dragData.dragEnd, false);
            this.element.addEventListener('touchmove', this.dragData.drag, false);

            this.element.addEventListener('mousedown', this.dragData.dragStart, false);
            this.element.addEventListener('mouseup', this.dragData.dragEnd, false);
            this.element.addEventListener('mousemove', this.dragData.drag, false);
        } else {
            this.element.removeEventListener('touchstart', this.dragData.dragStart, false);
            this.element.removeEventListener('touchend', this.dragData.dragEnd, false);
            this.element.removeEventListener('touchmove', this.dragData.drag, false);

            this.element.removeEventListener('mousedown', this.dragData.dragStart, false);
            this.element.removeEventListener('mouseup', this.dragData.dragEnd, false);
            this.element.removeEventListener('mousemove', this.dragData.drag, false);
        }
    }

    zIndex(z) {
        this.element.style.zIndex = z;
    }
    
    setImage() {
        if (this.reversed) {
            this.image.setAttribute('src', 'cards/back.svg');
        } else {
            this.image.setAttribute('src', 'cards/' + this.valueToString() + this.suit + '.svg');
        }
    }
    
    setVisibility() {
        if (this.hidden) {
            this.element.style.visibility = 'hidden';
        } else {
            this.element.style.visibility = 'visible';
        }
    }
    
    setLocation() {
        this.element.style.left = this.x + 'px';
        this.element.style.top = this.y + 'px';
    }
    
    relocate(x, y, relative) {
        if (relative) {
            this.x += x;
            this.y += y;
        } else {
            this.x = x;
            this.y = y;
        }
        this.setLocation();
    }
    
    move(x, y, time, relative) {  // todo: make it take a generator so it can do non-linear interpolation
        let xAbsolute = x;
        let yAbsolute = y;
        if (relative) {
            xAbsolute += this.x;
            yAbsolute += this.y;
        } else {
            x = x - this.x;
            y = y - this.y;
        }
        const xIncrement = x / time * frameTime;
        const yIncrement = y / time * frameTime;
        let count = 0;
        const moveCard = this.relocate.bind(this, xIncrement, yIncrement, true);
        let setIntervalID = undefined;
        const stopMovement = function() {
            clearInterval(setIntervalID);
        }
        setIntervalID = setInterval(() => {
            moveCard();
            count++;
            if (count >= Math.floor(time / frameTime)) {
                this.relocate(xAbsolute, yAbsolute, false);
                stopMovement();
            }
        }, frameTime);
    }
    
    reverse() {
        this.reversed = !this.reversed;
        this.setImage();
    }
    
    valueToString() {
        switch (this.value) {
            case 1:
                return "A";
            case 11:
                return "J";
            case 12:
                return "Q";
            case 13:
                return "K";
            default:
                return this.value.toString();
        }
    }
    
    destructor() {
        delete this.suit
        delete this.value
        delete this.id
        delete this.element
    }
    
};
Card.idGenerator = countUp(0);
Card.suits = ['S', 'H', 'D', 'C'];


const createDeck = function(reversed) {
    const deck = {};
    for (let i = 0; i < Card.suits.length; i++) {
        deck[Card.suits[i]] = [];
        for (let value = 1; value <= 13; value++) {
            deck[Card.suits[i]].push(new Card(Card.suits[i], value, reversed, true)); 
        }
    }
    return deck;
};


const SpiderSolitaire = class {
    
    constructor(suit, value, reverse) {
        document.body.style.backgroundColor = "green";
        this.decks = [createDeck(true), createDeck(true)];
        this.stacks = [...Array(10)].map(x => []);
        this.deals = [];
        this.dealCallback = this.deal.bind(this);
    }
    
    async play() {
        await this.reset();
    }
    
    async reset() {

        // Decide where cards go

        const order = [];
        for (const deck of this.decks) {
            for (const suit in deck) {
                for (const card of deck[suit]) {
                    order.push(card);
                }
            }
        }
        shuffleArray(order);
        
        for (let i = 0; i < order.length; i++) {
            if (i < 54) {
                this.stacks[i % 10].push(order[i]);
            } else {
                const j = Math.floor((i - 54) / 10);
                if (this.deals[j] === undefined) {
                    this.deals[j] = [];
                }
                this.deals[j].push(order[i]);
            }
        }
        
        // Actually move cards

        let i;
        for (i = 0; i < this.deals.length; i++) {
            for (let j = 0; j < this.deals[i].length; j++) {
                this.deals[i][j].hidden = false;
                this.deals[i][j].setVisibility();
                this.deals[i][j].relocate(screen.width - 150, screen.height - 300 - 10 * i, false);
                this.deals[i][j].zIndex(i);
                this.deals[i][j].element.addEventListener('click', this.dealCallback);
            }
        }
        for (const stack of this.stacks) {
            for (let j = 0; j < stack.length; j++) {
                stack[j].hidden = false;
                stack[j].setVisibility();
                stack[j].relocate(screen.width - 150, screen.height - 300 - 10 * i, false);
                stack[j].zIndex(i);
            }
        }

        for (const i of countUp(0)) {
            let count = 0;
            for (let j = 0; j < this.stacks.length; j++) {
                const card = this.stacks[j][i];
                if (card !== undefined) {
                    count++;
                    card.move(30 + 120 * j, 30 + 20 * i, 500, false);
                    card.zIndex(i);
                    card.element.removeEventListener('click', this.dealCallback);
                    await sleep(50);
                }
            }
            if (count == 0) {
                break;
            }
        }
        for (const stack of this.stacks) {
            const card = stack[stack.length - 1];
            await sleep(50);
            this.revealCard(card);
        }

    }

    cardMoved(card) {
        // todo: make more efficient
        // swtich from transform to position
        const xOffset = card.dragData.xOffset;
        const yOffset = card.dragData.yOffset;
        card.cascadeRelocate(xOffset, yOffset);
        card.clearTransform();

        // game logic

        // todo: make algo more efficient and less dumb
        let closestStackIndex;
        {
            let closestDistance = 500;
            for (let i = 0; i < this.stacks.length; i++) {
                const distance = Math.abs(card.x - (30 + 120 * i));
                if (distance < closestDistance) {
                    closestStackIndex = i;
                    closestDistance = distance;
                }
            }
        }
        const closestStack = this.stacks[closestStackIndex];

        if (closestStack[closestStack.length - 1].value == card.value + 1) {  // legal position

            // todo: edge case: moved to same stack it was already on
            const cardUnderNew = closestStack[closestStack.length - 1];
            if (cardUnderNew === undefined) {
            } else if (cardUnderNew.suit == card.suit) {  // actual stack
                cardUnderNew.attachCard(card);
            } else {
                for (const card of closestStack) {
                    card.toggleDraggable(false);
                }
            }

            const oldStack = this.moveCardStack(card, closestStackIndex);
            
            const cardUnderOld = oldStack[oldStack.length - 1];
            if (cardUnderOld.reversed) {
                this.revealCard(cardUnderOld);
            } else {
                cardUnderOld.toggleDraggable(true, this.cardMoved.bind(this));
                cardUnderOld.detachCard();
                for (let i = oldStack.length - 2; oldStack[i].dragData.attachedCard === oldStack[i + 1]; i--) {
                    oldStack[i].toggleDraggable(true, this.cardMoved.bind(this));
                }
            }
            
        } else {
            // snap back
            card.cascadeRelocate(-xOffset, -yOffset);
        }
        
    }

    moveCardStack(topCard, newStackIndex) {
        let oldStack;
        for (const stack of this.stacks) {
            if (stack.includes(topCard)) {
                oldStack = stack;
                break;
            }
        }
        if (oldStack === undefined) {
            console.error('unexpected error');
        }

        let i;
        for (i = 0; i < oldStack.length; i++) {
            if (oldStack[i] === topCard) {
                break;
            }
        }
        while (true) {
            if (oldStack[i] === undefined) {
                break;
            }
            const [card] = oldStack.splice(i, 1);

            card.relocate(30 + 120 * newStackIndex, 30 + 20 * this.stacks[newStackIndex].length, false);
            card.zIndex(this.stacks[newStackIndex].length);
            this.stacks[newStackIndex].push(card);

        }
        return oldStack;
    }

    revealCard(card) {
        card.reverse();
        card.toggleDraggable(true, this.cardMoved.bind(this));
    }

    checkWin() {
        // check if a full stack has been made
        for (const stack of this.stacks) {
            const end = stack.slice(stack.length - 13, stack.length);
            if (end.lengh < 13) {
                continue;
            }
            const suit = end[0].suit;
            let fullStack = true;
            for (let i = 13; i >= 1; i--) {
                if (end[i].suit != suit) {
                    fullStack = false;
                    break;
                }
                if (end[13 - i].value != i) {
                    fullStack = false;
                    break;
                }
            }
            if (fullStack) {
                const good = stack.splice(stack.length - 13);
                for (const card of good) {
                    card.hidden = true;
                    card.setVisibility();
                }
                break;
            }
        }

        // check if there are no cards left
    }
    
    async deal() {
        const deal = this.deals.pop();
        for (let i = 0; i < deal.length; i++) {
            const cardUnder = this.stacks[i][this.stacks[i].length - 1];
            if (cardUnder.value == deal[i].value - 1) {
                if (cardUnder.suit == deal[i].suit) {
                    cardUnder.attachCard(deal[i]);
                }
            } else {
                for (const card of this.stacks[i]) {
                    card.toggleDraggable(false);
                }
            }

            deal[i].move(30 + 120 * i, 30 + 20 * this.stacks[i].length, 500, false);
            deal[i].zIndex(this.stacks[i].length);

            await sleep(50);
            this.stacks[i].push(deal[i]);

            deal[i].element.removeEventListener('click', this.dealCallback);
            this.revealCard(deal[i]);
        }
    }
    
};
